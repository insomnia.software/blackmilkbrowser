using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace BlackMilkBrowser
{
    public interface IDataRespository
    {
        List<Item> GetItems();
        bool UpdateItems(ICollection<Item> allItems, List<Item> newItems);
        bool WriteItemsToDB(List<Item> items);
        bool ClearDB();
    }
}