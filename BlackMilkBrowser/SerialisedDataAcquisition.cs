﻿using System.Collections.Generic;
using BlackMilkBrowser.Properties;

namespace BlackMilkBrowser
{
    class SerialisedDataAcquisition : IDataAcquisition
    {
        private bool firstCall = true;
        public List<Item> AcquireItems()
        {
            if (firstCall)
            {
                firstCall = false;
                return Serialisation.SerialiseIn<List<Item>>($"{Settings.Default.DataFolder}storageNew.xml");
            }
                return Serialisation.SerialiseIn<List<Item>>($"{Settings.Default.DataFolder}storageNew.xml");
        }
    }
}
