﻿using System.Collections.Generic;
using System.Linq;
using NodaTime;

namespace BlackMilkBrowser
{
    public class Item
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Url { get; set; }
        public List<string> ImageUrls { get; set; }
        public double Price { get; set; }
        public string Currency { get; set; }
        public List<Availability> Availabilities { get; set; }
        public List<string> Collections { get; set; }

        public override string ToString() => $"Id: {Id}, Name: {Name}, Url: {Url}, ImageUrls: {ImageUrls}, Price: {Price}, Currency: {Currency}, Availabilities: {PrintAvailabilities()}, Collections: {Collections}";

        public string PrintAvailabilities() => Availabilities.Aggregate("", (current, availability) => current + (availability.ToString() + ""));
    }
}
