﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using BlackMilkBrowser.Properties;
using NodaTime;

namespace BlackMilkBrowser
{
    public class HtmlDataAquisition : IDataAcquisition
    {
        private readonly string data = Settings.Default.DataFolder;
        public List<Item> AcquireItems()
        {
            IEnumerable<string> lines = File.ReadLines($"{data}pages.txt");

            Instant pullTime = SystemClock.Instance.Now;
            List<Item> items = new List<Item>();
            foreach (string url in lines)
            {
                Debug.WriteLine($"Processing page {url}");
                bool productsOnPage = true; //check if we're on a valid page and not a 404
                int pageNumber = 1;

                while (productsOnPage)
                {
                    Debug.WriteLine($"Getting page number {pageNumber}");
                    string html = "";
                    try
                    {
                        html = GetHtml($"{url}?page={pageNumber}");
                    }
                    catch (WebException)
                    {
                        break;
                    }

                    if (Regex.IsMatch(html, "Sorry, there are no products in this collection"))
                    {
                        productsOnPage = false;
                        Debug.WriteLine($"Finished on page {pageNumber}");
                    }

                    var wholeItem = Regex.Matches(html, "<div class=\"grid__item large--one-(?:third|sixth) medium--one-(?:half|third)(?: small--one-half )? ?(?: sold-out)?(?: first| last|) collion-grid-item-product\">(.|\n)+?\\s+</div>"); //this regex returns a section of the html where every sectioon describes a single item

                    foreach (Match match in wholeItem)
                    {
                        Match nameAndUrl = Regex.Match(match.ToString(), "<a href=\"(.+?)\">(.+?)</a>");
                        string itemUrl = nameAndUrl.Groups[1].ToString();
                        string itemName = nameAndUrl.Groups[2].ToString();
                        string collection = Regex.Match(url, "/([a-z-]+)$").Groups[1].ToString();
                        if(collection == "Museum") Debugger.Break();
                        collection = char.ToUpper(collection[0]) + collection.Substring(1);
                        if (collection != "Museum")////museum pages are different in format, e.g. don't contain price and availability info
                        {
                            List<string> images = (from Match match2 in Regex.Matches(match.ToString(), "<source (?:data-)?srcset=\"(//cdn.shopify.com.+?1024x1024.+?)\"") select match2.Groups[1].ToString()).ToList(); //get the 2 images linked to the item

                            Match priceMatch = Regex.Match(match.ToString(), "<span class='money'>\\$([0-9]+\\.[0-9]+) ([A-Z]{3})");
                            double price = double.Parse(priceMatch.Groups[1].ToString());
                            string currency = priceMatch.Groups[2].ToString();

                            Debug.Write($"Item {itemName}, ${price}{currency} Available in: ");

                            List<Availability> availabilities = new List<Availability>();
                            string availability = Regex.Match(match.ToString(), "<div class=\"product-size-options-and-stock\">(.|\\n)+?</div>").ToString();
                            var availabilityMatches = Regex.Matches(availability, "<span( class=\"lowstock\")?>([A-Z ]+)</span>");
                            foreach (Match match2 in availabilityMatches)
                            {
                                string size = match2.Groups[2].ToString();
                                bool lowStock = !string.IsNullOrEmpty(match2.Groups[1].ToString());

                                availabilities.Add(new Availability { Size = (Size)Enum.Parse(typeof(Size), size.Replace(" ", "")), LowStock = lowStock, TimeAcquired = SystemClock.Instance.Now});

                                Debug.Write($"{size} ");
                                if (lowStock)
                                    Debug.Write("(L) ");
                            }
                            if (availabilityMatches.Count == 0)
                            {
                                availabilities.Add(new Availability {LowStock = false, Size = Size.SoldOut, TimeAcquired = SystemClock.Instance.Now});
                            }
                            Item itemExists = items.FirstOrDefault(x => x.Name == itemName);
                            if (itemExists != null)
                                itemExists.Collections.Add(collection);
                            else
                                items.Add(new Item { ImageUrls = images, Price = price, Currency = currency, Name = itemName, Url = itemUrl, Availabilities = availabilities, Collections = new List<string> { collection }});
                        }
                        else
                        {
                            List<string> images = (from Match match2 in Regex.Matches(match.ToString(), "<source (?:data-)?srcset=\"(//cdn.shopify.com.+?)\"") select match2.Groups[1].ToString()).ToList(); //get the 2 images linked to the item
                            Item itemExists = items.FirstOrDefault(x => x.Name == itemName);
                            if (itemExists != null)
                                itemExists.Collections.Add(collection);
                            else
                                items.Add(new Item { ImageUrls = images, Name = itemName, Url = itemUrl, Collections = new List<string> { collection }, Availabilities = new List<Availability> {new Availability {LowStock = false, Size = Size.SoldOut, TimeAcquired = SystemClock.Instance.Now} } });

                            Debug.Write($"Item {itemName} SOLD OUT - NOT AVAILABLE :(");
                        }
                        Debug.WriteLine("");
                        //Thread.Sleep(100);
                    }

                    Debug.WriteLine($"Found {wholeItem.Count} items");
                    pageNumber++;
                }
            }

            return items;
        }

        public string GetHtml(string url)
        {
            HttpWebRequest imageGetRequest = (HttpWebRequest)WebRequest.Create(url);
            imageGetRequest.Method = WebRequestMethods.Http.Get;
            imageGetRequest.UserAgent = "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/535.2 (KHTML, like Gecko) Chrome/15.0.874.121 Safari/535.2";
            imageGetRequest.AllowWriteStreamBuffering = true;
            imageGetRequest.ProtocolVersion = HttpVersion.Version11;
            imageGetRequest.AllowAutoRedirect = true;
            imageGetRequest.ContentType = "application/x-www-form-urlencoded";

            HttpWebResponse imageGetResponse = (HttpWebResponse)imageGetRequest.GetResponse();

            using (StreamReader sr = new StreamReader(imageGetResponse.GetResponseStream()))
            {
                return sr.ReadToEnd();
            }
        }

    }
}
