﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SQLite;
using System.Diagnostics;
using System.Linq;
using System.Text.RegularExpressions;

namespace BlackMilkBrowser
{
    public class SqliteData : IDisposable, IDataRespository
    {
        private readonly SQLiteConnection mySqlConnection;
        private int nextId;
        public SqliteData(string dbLocation)
        {
            mySqlConnection = new SQLiteConnection($"Data Source={dbLocation}");
            mySqlConnection.Open();
        }

        public List<Item> GetItems()
        {
            List<Item> items = new List<Item>();

            string sql = "SELECT Id, Name, Url, Price, Currency from Items;";

            using (SQLiteCommand cmd = new SQLiteCommand(sql, mySqlConnection))
            {
                using (SQLiteDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        Item item = new Item
                        {
                            Id = reader.GetInt32(0),
                            Name = reader.GetString(1),
                            Url = reader.GetString(2),
                            Price = reader.GetDouble(3),
                            Currency = reader.GetString(4)
                        };
                        items.Add(item);
                    }
                }
            }

            sql = "SELECT ItemId, ImageUrl from ImageUrls;";

            using (SQLiteCommand cmd = new SQLiteCommand(sql, mySqlConnection))
            {
                using (SQLiteDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        Item i = items.FirstOrDefault(item => item.Id == reader.GetInt32(0));

                        if (i != null)
                        {
                            if (i.ImageUrls == null)
                                i.ImageUrls = new List<string>();

                            i.ImageUrls.Add(reader.GetString(1));
                        }
                    }
                }
            }

            sql = "SELECT ItemId, Collection from Collections;";

            using (SQLiteCommand cmd = new SQLiteCommand(sql, mySqlConnection))
            {
                using (SQLiteDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        Item i = items.FirstOrDefault(item => item.Id == reader.GetInt32(0));

                        if (i != null)
                        {
                            if (i.Collections == null)
                                i.Collections = new List<string>();

                            i.Collections.Add(reader.GetString(1));
                        }
                    }
                }
            }

            sql = "SELECT ItemId, Size, LowStock, Time from Availabilities;";

            using (SQLiteCommand cmd = new SQLiteCommand(sql, mySqlConnection))
            {
                using (SQLiteDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        Item i = items.FirstOrDefault(item => item.Id == reader.GetInt32(0));

                        if (i != null)
                        {
                            if (i.Availabilities == null)
                                i.Availabilities = new List<Availability>();

                            int j = reader.GetInt32(1);
                            i.Availabilities.Add(new Availability
                            {
                                Size = (Size)j,
                                LowStock = (reader.GetInt32(2) == 1),
                                TimeAcquired = new NodaTime.Instant(reader.GetInt64(3))
                            });
                        }
                    }
                }
            }

            return items;

        }

        public bool UpdateItems(ICollection<Item> allItems, List<Item> newItems)
        {
            List<Item> existingItems = GetItems();
            SetHighestInt();

            foreach (Item existingItem in existingItems)
            {
                foreach (Item newItem in newItems)
                {
                    if (existingItem.Name == newItem.Name)
                    {
                        string sql = "";
                        sql = newItem.Availabilities.Aggregate(sql,
                (current, availability) =>
                    current +
                    $"INSERT into Availabilities (ItemId, Size, LowStock, Time) VALUES ({nextId}, {((int)(availability.Size))}, {Convert.ToInt32(availability.LowStock)}, {availability.TimeAcquired.Ticks});");
                    }
                    else
                    {
                        WriteItemToDB(newItem);
                    }
                }
            }

            return false;
        }

        public bool WriteItemsToDB(List<Item> items)
        {
            if (!SetHighestInt())
                return false;

            try
            {
                SQLiteCommand begin = new SQLiteCommand("begin", mySqlConnection);
                begin.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
                return false;
            }

            foreach (Item item in items)
                WriteItemToDB(item);

            try
            {
                SQLiteCommand end = new SQLiteCommand("end", mySqlConnection);
                end.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
                return false;
            }

            return true;
        }

        public bool WriteItemToDB(Item item)
        {
            string sql = $"INSERT into Items (Id, Name, Url, Price, Currency) VALUES ({nextId}, \"{item.Name}\", \"{item.Url}\", {item.Price}, \"{item.Currency}\");";

            sql = item.ImageUrls.Aggregate(sql,
                (current, url) => current + $"INSERT into ImageUrls (ItemId, ImageUrl) VALUES ({nextId}, \"{url}\");");

            sql = item.Collections.Aggregate(sql,
                (current, collection) =>
                    current + $"INSERT into Collections (ItemId, Collection) VALUES ({nextId}, \"{collection}\");");

            //if (item.Availabilities != null)
            sql = item.Availabilities.Aggregate(sql,
                (current, availability) =>
                    current +
                    $"INSERT into Availabilities (ItemId, Size, LowStock, Time) VALUES ({nextId}, {((int)(availability.Size))}, {Convert.ToInt32(availability.LowStock)}, {availability.TimeAcquired.Ticks});");
            //else
            //	sql += $"INSERT into Availabilities (ItemId, Size, LowStock) VALUES ({nextId}, \"Sold Out - Not Available :(\", 0);";

            SQLiteCommand command = new SQLiteCommand(sql, mySqlConnection);
            try
            {
                command.ExecuteNonQuery();
                nextId++;
            }
            catch (Exception e)
            {
                Debug.Write(e.Message);
                SQLiteCommand end = new SQLiteCommand("end", mySqlConnection);
                end.ExecuteNonQuery();
                return false;
            }

            return true;
        }

        public bool ClearDB()
        {
            string sql = "Delete from Items; Delete from imageUrls; delete from collections; delete from availabilities;";

            SQLiteCommand command = new SQLiteCommand(sql, mySqlConnection);
            try
            {
                command.ExecuteNonQuery();
                nextId++;
            }
            catch (Exception e)
            {
                Debug.Write(e.Message);
                return false;
            }

            return true;
        }

        public bool SetHighestInt()
        {
            string sql = "select Id from items order by Id desc limit 1";

            SQLiteCommand command = new SQLiteCommand(sql, mySqlConnection);
            SQLiteDataReader reader;
            try
            {
                reader = command.ExecuteReader();
            }
            catch (Exception e)
            {
                Debug.Write(e.Message);
                return false;
            }

            if (reader != null && reader.HasRows)
            {
                while (reader.Read())
                {
                    nextId = reader.GetInt32(0) + 1;
                    return true;
                }
            }

            nextId = 1;
            return true;
        }

        public void Dispose() => mySqlConnection.Close();
    }
}
