﻿namespace BlackMilkBrowser
{
    public enum Size
    {
        XXS,
        XS,
        S,
        M,
        L,
        XL,
        XXL,
        LTXXS,
        LTXS,
        LTS,
        LTM,
        LTL,
        LTXL,
        OSFA,
        OSFM,
        SoldOut
    }
}
