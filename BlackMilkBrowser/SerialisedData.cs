﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using BlackMilkBrowser.Properties;

namespace BlackMilkBrowser
{
    public class SerialisedData : IDataRespository
    {
        public List<Item> GetItems() => Serialisation.SerialiseIn<List<Item>>($"{Settings.Default.DataFolder}storage.xml");

        public bool UpdateItems(ICollection<Item> allItems, List<Item> newItems)
        {
            IEnumerable<Item> diff = allItems.Except(newItems, new ItemComparer());
            diff = diff.Concat(newItems.Except(allItems, new ItemComparer()));


            IEnumerable<Item> items = diff as IList<Item> ?? diff.ToList();

            Debug.WriteLine(items.Count(x => x.Name == "A Whole New World Leggings - LIMITED"));

            if (!items.Any())
                return true;//no difference, so we've successfully updated

            foreach (Item item in items)
            {
                foreach (Item allItem in allItems)
                {
                    if(item.Name == allItem.Name)
                        Debug.WriteLine(allItem.ToString());
                }

                foreach (Item newItem in newItems)
                {
                    if (item.Name == newItem.Name)
                        Debug.WriteLine(newItem.ToString());
                }
            }

            foreach (Item item in items)
            {
                foreach (Item allItem in allItems)
                {
                    if (allItem.Name == item.Name)
                    {
                        if(allItem.Name == "Bambi Reversible Crop - LIMITED")
                            Console.WriteLine();

                        IEnumerable<Availability> v = allItem.Availabilities.Except(item.Availabilities, new AvailabilityComparer()).Concat(item.Availabilities.Except(allItem.Availabilities, new AvailabilityComparer()));

                        foreach (Availability availability in v)
                        {
                            if(!allItem.Availabilities.Contains(availability))
                                allItem.Availabilities.Add(availability);
                        }
                    }
                }
            }

            return true;
        }

        public bool WriteItemsToDB(List<Item> items)
        {
            Serialisation.SerialiseOut(items, $"{Settings.Default.DataFolder}storage.xml");
            return true;
        }

        public bool ClearDB() => true;
    }

    public class AvailabilityComparer : IEqualityComparer<Availability>
    {
        public bool Equals(Availability x, Availability y) => x?.Size == y?.Size && x?.LowStock == y?.LowStock;

        public int GetHashCode(Availability obj) => obj.Size.GetHashCode() ^ obj.LowStock.GetHashCode();
    }

    public class ItemComparer : IEqualityComparer<Item>
    {
        public bool Equals(Item x, Item y)
        {
            if (x.Name == y.Name)
            {
                IEnumerable<Availability> dif = x.Availabilities.Except(y.Availabilities, new AvailabilityComparer());
                if (!dif.Any())
                    return true;
            }
            return false;
        }

        public int GetHashCode(Item obj)
        {
            int hash = obj.Name.GetHashCode();

            hash = obj.Availabilities.Aggregate(hash, (current, availability) => current ^ availability.GetHashCode());

            return hash;
        }
    }
}
