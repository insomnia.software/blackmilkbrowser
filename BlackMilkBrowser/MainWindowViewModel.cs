﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Xml.Serialization;
using BlackMilkBrowser.Properties;

namespace BlackMilkBrowser
{
    public class MainWindowViewModel : INotifyPropertyChanged
    {
        private readonly string data = Settings.Default.DataFolder;

        private readonly IDataAcquisition dataAcquisition = new SerialisedDataAcquisition();
        private readonly IDataRespository dataRespository = new SerialisedData();

        //private readonly SqliteData db = new SqliteData($"{Settings.Default.DataFolder}BlackMilk.db");
        private readonly Currency currency = new Currency();
        private ObservableCollection<Item> items;

        private string searchBox;
        public string SearchBox { get { return searchBox; } set { searchBox = value; OnPropertyChanged(); } }

        private ObservableCollection<ItemViewModel> searchResults = new ObservableCollection<ItemViewModel>();
        public ObservableCollection<ItemViewModel> SearchResults { get { return searchResults; } set { searchResults = value; OnPropertyChanged(); } }

        private ItemViewModel selectedItemSearchResults;

        public ItemViewModel SelectedItemSearchResults
        {
            get
            {
                return selectedItemSearchResults;
            }
            set
            {
                if (selectedItemSearchResults != value)
                {
                    selectedItemSearchResults = value;
                    OnPropertyChanged();

                    if (value == null)
                    {
                        Image2 = Image1 = new BitmapImage();
                        return;
                    }

                    UpdatePriceConversion(value.Price);

                    //if (value.ImageUrls?.Count > 0)
                    //    Image1 = Example($"http:{value.ImageUrls[0]}").Result;
                    //if (value.ImageUrls?.Count > 1)
                    //    Image2 = Example($"http:{value.ImageUrls[1]}").Result;

                    if (value.ImageUrls?.Count > 0)
                    {
                        //DownloadRemoteImageFile($"http:{value.ImageUrls[0]}", $@"{data}Images\{value.Name}1.jpg");
                        //Image1 = new BitmapImage(new Uri($@"{data}Images\{value.Name}1.jpg"));
                        Image1 = new BitmapImage(new Uri($"http:{value.ImageUrls[0]}"));
                    }
                    if (value.ImageUrls?.Count > 1)
                    {
                        //DownloadRemoteImageFile($"http:{value.ImageUrls[1]}",$@"{data}Images\{value.Name}2.jpg");
                        //Image2 = new BitmapImage(new Uri($@"{data}Images\{value.Name}2.jpg"));
                        Image2 = new BitmapImage(new Uri($"http:{value.ImageUrls[1]}"));
                    }
                }
            }
        }

        private async Task<BitmapImage> Example(string s) => await Task.Run(() => new BitmapImage(new Uri(s)));

        private void UpdatePriceConversion(double value)
        {
            string currSymbol;
            if (TryGetCurrencySymbol(SelectedCurrency, out currSymbol))
            {
                double? convertedAud = currency.ConvertAud(SelectedCurrency, value);
                if (convertedAud != null)
                    SelectedItemPriceConversionString = $"{currSymbol}{convertedAud:0.00}";
            }
        }

        private string selectedItemPriceConversionString;
        public string SelectedItemPriceConversionString { get { return selectedItemPriceConversionString; } set { selectedItemPriceConversionString = value; OnPropertyChanged(); } }

        private ObservableCollection<string> currencies = new ObservableCollection<string> { "AED", "AFN", "ALL", "AMD", "ANG", "AOA", "ARS", "AUD", "AWG", "AZN", "BAM", "BBD", "BDT", "BGN", "BHD", "BIF", "BMD", "BND", "BOB", "BRL", "BSD", "BTC", "BTN", "BWP", "BYR", "BZD", "CAD", "CDF", "CHF", "CLF", "CLP", "CNY", "COP", "CRC", "CUC", "CUP", "CVE", "CZK", "DJF", "DKK", "DOP", "DZD", "EEK", "EGP", "ERN", "ETB", "EUR", "FJD", "FKP", "GBP", "GEL", "GGP", "GHS", "GIP", "GMD", "GNF", "GTQ", "GYD", "HKD", "HNL", "HRK", "HTG", "HUF", "IDR", "ILS", "IMP", "INR", "IQD", "IRR", "ISK", "JEP", "JMD", "JOD", "JPY", "KES", "KGS", "KHR", "KMF", "KPW", "KRW", "KWD", "KYD", "KZT", "LAK", "LBP", "LKR", "LRD", "LSL", "LTL", "LVL", "LYD", "MAD", "MDL", "MGA", "MKD", "MMK", "MNT", "MOP", "MRO", "MTL", "MUR", "MVR", "MWK", "MXN", "MYR", "MZN", "NAD", "NGN", "NIO", "NOK", "NPR", "NZD", "OMR", "PAB", "PEN", "PGK", "PHP", "PKR", "PLN", "PYG", "QAR", "RON", "RSD", "RUB", "RWF", "SAR", "SBD", "SCR", "SDG", "SEK", "SGD", "SHP", "SLL", "SOS", "SRD", "STD", "SVC", "SYP", "SZL", "THB", "TJS", "TMT", "TND", "TOP", "TRY", "TTD", "TWD", "TZS", "UAH", "UGX", "USD", "UYU", "UZS", "VEF", "VND", "VUV", "WST", "XAF", "XAG", "XAU", "XCD", "XDR", "XOF", "XPD", "XPF", "XPT", "YER", "ZAR", "ZMK", "ZMW", "ZWL" };
        public ObservableCollection<string> Currencies { get { return currencies; } set { currencies = value; OnPropertyChanged(); } }

        private string selectedCurrency = "GBP";

        public string SelectedCurrency
        {
            get
            {
                return selectedCurrency;
            }
            set
            {
                selectedCurrency = value;
                OnPropertyChanged();
                UpdatePriceConversion(SelectedItemSearchResults.Price);
            }
        }

        private ObservableCollection<string> catagories = new ObservableCollection<string> { "All", "Catsuits", "Dresses", "Gym", "Hosiery", "Jackets", "Leggings", "Limited", "Museum", "New", "Playsuits", "Shorts", "Skirts", "Suspenders", "Swimsuits", "Tops" };
        public ObservableCollection<string> Catagories { get { return catagories; } set { catagories = value; OnPropertyChanged(); } }

        private string selectedCatagory = "All";
        public string SelectedCatagory { get { return selectedCatagory; } set { selectedCatagory = value; OnPropertyChanged(); } }

        private bool includeMuseumChecked;

        public bool IncludeMuseumChecked
        {
            get
            {
                return includeMuseumChecked;
            }
            set
            {
                includeMuseumChecked = value;
                OnPropertyChanged();
                if (value)
                {
                    SizeComboBoxOneEnabled = false;
                    SelectedSizeOne = "";
                    SizeComboBoxTwoEnabled = false;
                    SelectedSizeTwo = "";
                    SizeComboBoxThreeEnabled = false;
                    SelectedSizeThree = "";
                    SizeComboBoxFourEnabled = false;
                    SelectedSizeFour = "";
                    SizeComboBoxFiveEnabled = false;
                    SelectedSizeFive = "";
                }
                else
                {
                    SizeComboBoxOneEnabled = true;
                }
            }
        }

        private ObservableCollection<string> sizes = new ObservableCollection<string> { "", "XXS", "XS", "S", "M", "L", "XL", "LTXXS", "LTXS", "LTS", "LTM", "LTL", "LTXL", "OSFA", "OSFM", "SoldOut" };
        public ObservableCollection<string> Sizes { get { return sizes; } set { sizes = value; OnPropertyChanged(); } }

        private bool sizeComboBoxOneEnabled = true;
        public bool SizeComboBoxOneEnabled { get { return sizeComboBoxOneEnabled; } set { sizeComboBoxOneEnabled = value; OnPropertyChanged(); } }

        private bool sizeComboBoxTwoEnabled;
        public bool SizeComboBoxTwoEnabled { get { return sizeComboBoxTwoEnabled; } set { sizeComboBoxTwoEnabled = value; OnPropertyChanged(); } }

        private bool sizeComboBoxThreeEnabled;
        public bool SizeComboBoxThreeEnabled { get { return sizeComboBoxThreeEnabled; } set { sizeComboBoxThreeEnabled = value; OnPropertyChanged(); } }

        private bool sizeComboBoxFourEnabled;
        public bool SizeComboBoxFourEnabled { get { return sizeComboBoxFourEnabled; } set { sizeComboBoxFourEnabled = value; OnPropertyChanged(); } }

        private bool sizeComboBoxFiveEnabled;
        public bool SizeComboBoxFiveEnabled { get { return sizeComboBoxFiveEnabled; } set { sizeComboBoxFiveEnabled = value; OnPropertyChanged(); } }

        private string selectedSizeOne = "";

        public string SelectedSizeOne
        {
            get
            {
                return selectedSizeOne;
            }
            set
            {
                selectedSizeOne = value;
                OnPropertyChanged();
                if (value != "")
                {
                    SizeComboBoxTwoEnabled = true;
                }
                else
                {
                    SizeComboBoxTwoEnabled = false;
                    SelectedSizeTwo = "";
                    SizeComboBoxThreeEnabled = false;
                    SelectedSizeThree = "";
                    SizeComboBoxFourEnabled = false;
                    SelectedSizeFour = "";
                    SizeComboBoxFiveEnabled = false;
                    SelectedSizeFive = "";
                }
            }
        }

        private string selectedSizeTwo = "";

        public string SelectedSizeTwo
        {
            get
            {
                return selectedSizeTwo;
            }
            set
            {
                selectedSizeTwo = value;
                OnPropertyChanged();
                if (value != "")
                {
                    SizeComboBoxThreeEnabled = true;
                }
                else
                {
                    SizeComboBoxThreeEnabled = false;
                    SelectedSizeThree = "";
                    SizeComboBoxFourEnabled = false;
                    SelectedSizeFour = "";
                    SizeComboBoxFiveEnabled = false;
                    SelectedSizeFive = "";
                }
            }
        }

        private string selectedSizeThree = "";

        public string SelectedSizeThree
        {
            get
            {
                return selectedSizeThree;
            }
            set
            {
                selectedSizeThree = value;
                OnPropertyChanged();
                if (value != "")
                {
                    SizeComboBoxFourEnabled = true;
                }
                else
                {
                    SizeComboBoxFourEnabled = false;
                    SelectedSizeFour = "";
                    SizeComboBoxFiveEnabled = false;
                    SelectedSizeFive = "";
                }
            }
        }

        private string selectedSizeFour = "";

        public string SelectedSizeFour
        {
            get
            {
                return selectedSizeFour;
            }
            set
            {
                selectedSizeFour = value;
                OnPropertyChanged();
                if (value != "")
                {
                    SizeComboBoxFiveEnabled = true;
                }
                else
                {
                    SizeComboBoxFiveEnabled = false;
                    SelectedSizeFive = "";
                }
            }
        }

        private string selectedSizeFive = "";
        public string SelectedSizeFive { get { return selectedSizeFive; } set { selectedSizeFive = value; OnPropertyChanged(); } }

        private ImageSource image1;
        public ImageSource Image1 { get { return image1; } set { image1 = value; OnPropertyChanged(); } }

        private ImageSource image2;
        public ImageSource Image2{ get { return image2; } set{ image2 = value; OnPropertyChanged();} }

        private int searchResultCount;
        public int SearchResultCount { get { return searchResultCount; } set { searchResultCount = value; OnPropertyChanged(); } }

        private ICommand searchCommand;
        public ICommand SearchCommand => searchCommand ?? (searchCommand = new RelayCommand(param => Search()));

        private void Search()
        {
            SearchResults.Clear();
            if(items == null)
                items = new ObservableCollection<Item>(dataRespository.GetItems());

            List<Item> restrictedItems = RestrictToName(items.ToList(), SearchBox);

            List<string> sizesToSearch = new List<string>();
            if (!IncludeMuseumChecked)//if museum results excluded, then we want to search only available product, so include all sizes in the search
                sizesToSearch = GetSizesToSearch();//if we want museum results, we can't restrict by size at all since museum items don't have a size

            restrictedItems = RestrictToSize(restrictedItems, sizesToSearch);

            restrictedItems = RestrictToCollection(restrictedItems, new List<string> {SelectedCatagory});

            SearchResults = new ObservableCollection<ItemViewModel>(ConvertItemsToItemViewModels(restrictedItems, sizesToSearch));
            SearchResultCount = SearchResults.Count;
        }

        private List<Item> RestrictToName(List<Item> items, string nameToSearch)
        {
            if (string.IsNullOrEmpty(nameToSearch))//no name entered, return the list unrestricted
                return items;

            List<Item> restrictedItems = new List<Item>();

            foreach (Item item in items)
                if(item.Name.ToUpper().Contains(nameToSearch.ToUpper()))
                    restrictedItems.Add(item);

            return restrictedItems;
        }

        private List<Item> RestrictToCollection(List<Item> items, List<string> collectionsToSearch)
        {
            if (collectionsToSearch.Contains("All"))//searching all catagories, return the list unrestricted
                return items;

            List<Item> restrictedItems = new List<Item>();

            foreach (Item item in items)
                foreach (string collection in collectionsToSearch)
                    if(item.Collections.Contains(collection))
                        restrictedItems.Add(item);

            return restrictedItems;
        }
        private List<Item> RestrictToSize(List<Item> items, List<string> sizesToSearch)
        {
            if (sizesToSearch == null || sizesToSearch.Count == 0)
                return items;

            List<Item> restrictedItems = new List<Item>();
            foreach (Item item in items)
                foreach (Availability availability in item.Availabilities)
                    foreach (string size in sizesToSearch)
                        if (availability.Size == (Size)Enum.Parse(typeof(Size), size))
                            if (!restrictedItems.Contains(item))
                                restrictedItems.Add(item);

            return restrictedItems;
        }

        private List<ItemViewModel> ConvertItemsToItemViewModels(List<Item> items, List<string> sizesToSearch)
        {
            List<ItemViewModel> itemViewModels = new List<ItemViewModel>();

            foreach (Item item in items)
            {
                ItemViewModel ivm = null;
                if (item.Availabilities.Count == 0)
                {
                    ivm = new ItemViewModel { ItemColour = Brushes.Gray, Name = item.Name, ImageUrls = item.ImageUrls, Currency = item.Currency, Price = item.Price, Url = $"http://blackmilkclothing.com{item.Url}" };
                }
                else
                {
                    foreach (Availability availability in item.Availabilities)
                    {
                        if (itemViewModels.FirstOrDefault(x => x.Name == item.Name) != null) //prevent duplicates
                        {
                            if(sizesToSearch.Contains(availability.Size.ToString()))
                                if (availability.LowStock)//item is already in list, but it might have been added as a high stock item, but another size specified in the search it is low stock in, so set it to red
                                    itemViewModels.FirstOrDefault(x => x.Name == item.Name).ItemColour = Brushes.Red;
                            continue;
                        }

                        ivm = new ItemViewModel { ItemColour = availability.LowStock && sizesToSearch.Contains(availability.Size.ToString()) ? Brushes.Red : Brushes.Black, Size = availability.Size, Name = item.Name, ImageUrls = item.ImageUrls, Currency = item.Currency, Price = item.Price, Url = $"http://blackmilkclothing.com{item.Url}" };
                    }
                }
                ivm.SetAvailability(item.Availabilities);
                itemViewModels.Add(ivm);
            }

            itemViewModels.Sort();

            return itemViewModels;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>Returns sizes seected in combo boxes, or all sizes if no size selected</returns>
        private List<string> GetSizesToSearch()
        {
            List<string> sizesToSearch = new List<string>();
            if (SelectedSizeOne != "")
            {
                sizesToSearch.Add(SelectedSizeOne);
                if (SelectedSizeTwo != "")
                {
                    sizesToSearch.Add(SelectedSizeTwo);
                    if (SelectedSizeThree != "")
                    {
                        sizesToSearch.Add(SelectedSizeThree);
                        if (SelectedSizeFour != "")
                        {
                            sizesToSearch.Add(SelectedSizeFour);
                            if (SelectedSizeFive != "")
                                sizesToSearch.Add(selectedSizeFive);
                        }
                    }
                }
            }

            if (sizesToSearch.Count == 0)//if no size selected, search all sizes
            {
                sizesToSearch.AddRange(sizes.ToList().GetRange(1, sizes.Count - 1));//remove the empty string off the list as this is intentionally the first item in the combo box
                sizesToSearch.Remove("SoldOut");
            }

            return sizesToSearch;
        }

        private ObservableCollection<Item> GetAllItems()
        {
            ObservableCollection <Item> items;
            using (FileStream fs = new FileStream($"{data}storage.xml", FileMode.Open))
            {
                XmlSerializer xSer = new XmlSerializer(typeof(List<Item>));
                items = new ObservableCollection<Item>((List<Item>)(xSer.Deserialize(fs)));
            }

            return items;
        }

        private ICommand aquireCommand;
        public ICommand AquireCommand => aquireCommand ?? (aquireCommand = new RelayCommand(param => Aquire()));

        private void Aquire()
        {
            List<Item> allItems = dataAcquisition.AcquireItems();

            //dataRespository.ClearDB();
            dataRespository.WriteItemsToDB(allItems);
        }

        private ICommand updateCommand;
        public ICommand UpdateCommand => updateCommand ?? (updateCommand = new RelayCommand(param => Update()));

        private void Update()
        {
            List<Item> newItems = dataAcquisition.AcquireItems();

            dataRespository.UpdateItems(items, newItems);
        }

        private ICommand urlCommand;
        public ICommand UrlCommand => urlCommand ?? (urlCommand = new RelayCommand(param => GoToUrl()));

        private void GoToUrl() => Process.Start(SelectedItemSearchResults.Url);

        private void DownloadRemoteImageFile(string uri, string fileName)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();

            // Check that the remote file was found. The ContentType
            // check is performed since a request for a non-existent
            // image file might be redirected to a 404-page, which would
            // yield the StatusCode "OK", even though the image was not
            // found.
            if ((response.StatusCode == HttpStatusCode.OK ||
                response.StatusCode == HttpStatusCode.Moved ||
                response.StatusCode == HttpStatusCode.Redirect) &&
                response.ContentType.StartsWith("image", StringComparison.OrdinalIgnoreCase))
            {

                // if the remote file was found, download oit
                using (Stream inputStream = response.GetResponseStream())
                using (Stream outputStream = File.OpenWrite(fileName))
                {
                    byte[] buffer = new byte[4096];
                    int bytesRead;
                    do
                    {
                        bytesRead = inputStream.Read(buffer, 0, buffer.Length);
                        outputStream.Write(buffer, 0, bytesRead);
                    } while (bytesRead != 0);
                }
            }
        }

        public bool TryGetCurrencySymbol(string isoCurrencySymbol, out string symbol)
        {
            symbol = CultureInfo
                .GetCultures(CultureTypes.AllCultures)
                .Where(c => !c.IsNeutralCulture)
                .Select(culture => {
                    try
                    {
                        return new RegionInfo(culture.LCID);
                    }
                    catch (ArgumentException)
                    {
                        return null;
                    }
                })
                .Where(ri => ri != null && ri.ISOCurrencySymbol == isoCurrencySymbol)
                .Select(ri => ri.CurrencySymbol)
                .FirstOrDefault();
            return symbol != null;
        }

        #region INPC
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null) => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        #endregion
    }
}
