﻿using System.IO;
using System.Xml.Serialization;

namespace BlackMilkBrowser
{
    public static class Serialisation
    {
        public static void SerialiseOut<T>(T item, string filename)
        {
            XmlSerializer xs = new XmlSerializer(typeof(T));
            using (FileStream fs = new FileStream(filename, FileMode.Create))
            {
                xs.Serialize(fs, item);
            }
        }

        public static T SerialiseIn<T>(string filename)
        {
            using (FileStream fs = new FileStream(filename, FileMode.Open))
            {
                XmlSerializer xSer = new XmlSerializer(typeof(T));
                return (T)(xSer.Deserialize(fs));
            }
        }
    }
}
