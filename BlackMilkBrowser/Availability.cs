﻿using System;
using NodaTime;

namespace BlackMilkBrowser
{
    public class Availability : IEquatable<Availability>
    {
        public bool Equals(Availability other) => Size == other.Size && LowStock == other.LowStock;

        public override int GetHashCode()
        {
            unchecked
            {
                return ((int) Size*397) ^ LowStock.GetHashCode();
            }
        }

        public Size Size { get; set; }
        public bool LowStock { get; set; }
        public Instant TimeAcquired { get; set; }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((Availability) obj);
        }

        public override string ToString() => $"Size: {Size}, LowStock: {LowStock}";
    }
}
