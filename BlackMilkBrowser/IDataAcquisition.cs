﻿using System.Collections.Generic;

namespace BlackMilkBrowser
{
    public interface IDataAcquisition
    {
        List<Item> AcquireItems();
    }
}
