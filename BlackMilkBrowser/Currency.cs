﻿using System.Collections.Generic;
using System.IO;
using System.Net;
using Newtonsoft.Json;
using BlackMilkBrowser.Properties;

namespace BlackMilkBrowser
{
    public class Currency
    {
        private string json;
        public double? ConvertAud(string destinationCurrency, double value)
        {
            if (destinationCurrency.Length != 3 || value < 0)
                return null;

            if(json == null)//one method call per application run
                json = GetJson();

            Latest latest = JsonConvert.DeserializeObject<Latest>(json);

            double audToUsd = latest.Rates["AUD"];
            double imputToUsd = latest.Rates[destinationCurrency];

            double usd = value/audToUsd;
            double desiredCurrency = imputToUsd*usd;

            return desiredCurrency;
        }

        private string GetJson()
        {
			string apiKey = File.ReadAllText(Settings.Default.ApiKeyLocation);
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create($"https://openexchangerates.org/api/latest.json?app_id={apiKey}");//$"{Settings.Default.DataFolder}storage.xml"
			request.ContentType = "application/json; charset=utf-8";
            request.Method = WebRequestMethods.Http.Get;
            request.Accept = "application/json";
            string text;
            var response = (HttpWebResponse)request.GetResponse();

            var stream = response.GetResponseStream();

            if (stream == null)
                return null;

            using (var sr = new StreamReader(stream))
            {
                text = sr.ReadToEnd();
            }

            return text;
        }
    }

    public class Latest
    {
        public string Disclaimer { get; set; }
        public string License { get; set; }
        public long Timestamp { get; set; }
        public string Base { get; set; }
        public Dictionary<string, double> Rates { get; set; }
    }
}
