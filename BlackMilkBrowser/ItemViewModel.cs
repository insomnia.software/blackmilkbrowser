﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Media;
using BlackMilkBrowser.Annotations;

namespace BlackMilkBrowser
{
	public class ItemViewModel :  IComparer<ItemViewModel>, IComparable, INotifyPropertyChanged
	{
		public string Name { get; set; }
		public Size Size { get; set; }
		public List<string> ImageUrls { get; set; }
		public SolidColorBrush ItemColour { get; set; }
	    public double Price { get; set; }
	    public string Currency { get; set; }
	    public string Url { get; set; }
        public string PriceString => $"${Price:0.00} {Currency}";

        private Visibility xxsVisible = Visibility.Collapsed;
        public Visibility XxsVisible { get { return xxsVisible; } set { xxsVisible = value; OnPropertyChanged(nameof(XxsVisible)); } }

        private SolidColorBrush xxsColour = Brushes.Black;
        public SolidColorBrush XxsColour { get { return xxsColour; } set { xxsColour = value; OnPropertyChanged(nameof(XxsColour)); } }


        private Visibility xsVisible = Visibility.Collapsed;
        public Visibility XsVisible { get { return xsVisible; } set { xsVisible = value; OnPropertyChanged(nameof(XsVisible)); } }

        private SolidColorBrush xsColour = Brushes.Black;
        public SolidColorBrush XsColour { get { return xsColour; } set { xsColour = value; OnPropertyChanged(nameof(XsColour)); } }


        private Visibility sVisible = Visibility.Collapsed;
        public Visibility SVisible { get { return sVisible; } set { sVisible = value; OnPropertyChanged(nameof(SVisible)); } }

        private SolidColorBrush sColour = Brushes.Black;
        public SolidColorBrush SColour { get { return sColour; } set { sColour = value; OnPropertyChanged(nameof(SColour)); } }


        private Visibility mVisible = Visibility.Collapsed;
        public Visibility MVisible { get { return mVisible; } set { mVisible = value; OnPropertyChanged(nameof(MVisible)); } }

        private SolidColorBrush mColour = Brushes.Black;
        public SolidColorBrush MColour { get { return mColour; } set { mColour = value; OnPropertyChanged(nameof(MColour)); } }
        
        private Visibility lVisible = Visibility.Collapsed;
        public Visibility LVisible { get { return lVisible; } set { lVisible = value; OnPropertyChanged(nameof(LVisible)); } }

        private SolidColorBrush lColour = Brushes.Black;
        public SolidColorBrush LColour { get { return lColour; } set { lColour = value; OnPropertyChanged(nameof(LColour)); } }

        private Visibility xlVisible = Visibility.Collapsed;
        public Visibility XlVisible { get { return xlVisible; } set { xlVisible = value; OnPropertyChanged(nameof(XlVisible)); } }

        private SolidColorBrush xlColour = Brushes.Black;
        public SolidColorBrush XlColour { get { return xlColour; } set { xlColour = value; OnPropertyChanged(nameof(XlColour)); } }

        private Visibility xxlVisible = Visibility.Collapsed;
        public Visibility XxlVisible { get { return xxlVisible; } set { xxlVisible = value; OnPropertyChanged(nameof(XxlVisible)); } }

        private SolidColorBrush xxlColour = Brushes.Black;
        public SolidColorBrush XxlColour { get { return xxlColour; } set { xxlColour = value; OnPropertyChanged(nameof(XxlColour)); } }

        private Visibility ltxxsVisible = Visibility.Collapsed;
        public Visibility LtxxsVisible { get { return ltxxsVisible; } set { ltxxsVisible = value; OnPropertyChanged(nameof(LtxxsVisible)); } }

        private SolidColorBrush ltxxsColour = Brushes.Black;
        public SolidColorBrush LtxxsColour { get { return ltxxsColour; } set { ltxxsColour = value; OnPropertyChanged(nameof(LtxxsColour)); } }


        private Visibility ltxsVisible = Visibility.Collapsed;
        public Visibility LtxsVisible { get { return ltxsVisible; } set { ltxsVisible = value; OnPropertyChanged(nameof(LtxsVisible)); } }

        private SolidColorBrush ltxsColour = Brushes.Black;
        public SolidColorBrush LtxsColour { get { return ltxsColour; } set { ltxsColour = value; OnPropertyChanged(nameof(LtxsColour)); } }


        private Visibility ltsVisible = Visibility.Collapsed;
        public Visibility LtsVisible { get { return ltsVisible; } set { ltsVisible = value; OnPropertyChanged(nameof(LtsVisible)); } }

        private SolidColorBrush ltsColour = Brushes.Black;
        public SolidColorBrush LtsColour { get { return ltsColour; } set { ltsColour = value; OnPropertyChanged(nameof(LtsColour)); } }


        private Visibility ltmVisible = Visibility.Collapsed;
        public Visibility LtmVisible { get { return ltmVisible; } set { ltmVisible = value; OnPropertyChanged(nameof(LtmVisible)); } }

        private SolidColorBrush ltmColour = Brushes.Black;
        public SolidColorBrush LtmColour { get { return ltmColour; } set { ltmColour = value; OnPropertyChanged(nameof(LtmColour)); } }


        private Visibility ltlVisible = Visibility.Collapsed;
        public Visibility LtlVisible { get { return ltlVisible; } set { ltlVisible = value; OnPropertyChanged(nameof(LtlVisible)); } }

        private SolidColorBrush ltlColour = Brushes.Black;
        public SolidColorBrush LtlColour { get { return ltlColour; } set { ltlColour = value; OnPropertyChanged(nameof(LtlColour)); } }


        private Visibility ltxlVisible = Visibility.Collapsed;
        public Visibility LtxlVisible { get { return ltxlVisible; } set { ltxlVisible = value; OnPropertyChanged(nameof(LtxlVisible)); } }

        private SolidColorBrush ltxlColour = Brushes.Black;
        public SolidColorBrush LtxlColour { get { return ltxlColour; } set { ltxlColour = value; OnPropertyChanged(nameof(LtxlColour)); } }


        private Visibility osfaVisible = Visibility.Collapsed;
        public Visibility OsfaVisible { get { return osfaVisible; } set { osfaVisible = value; OnPropertyChanged(nameof(OsfaVisible)); } }

        private SolidColorBrush osfaColour = Brushes.Black;
        public SolidColorBrush OsfaColour { get { return osfaColour; } set { osfaColour = value; OnPropertyChanged(nameof(OsfaColour)); } }


        private Visibility osfmVisible = Visibility.Collapsed;
        public Visibility OsfmVisible { get { return osfmVisible; } set { osfmVisible = value; OnPropertyChanged(nameof(OsfmVisible)); } }

        private SolidColorBrush osfmColour = Brushes.Black;
        public SolidColorBrush OsfmColour { get { return osfmColour; } set { osfmColour = value; OnPropertyChanged(nameof(OsfmColour)); } }

	    public void SetAvailability(List<Availability> availabilities)
	    {
	        foreach (Availability availability in availabilities)
	        {
	            switch (availability.Size)
	            {
                    case Size.XXS:
                        XxsVisible = Visibility.Visible;
                        if (availability.LowStock)
                            XxsColour = Brushes.Red;
                        break;
                    case Size.XS:
                        XsVisible = Visibility.Visible;
                        if (availability.LowStock)
                            XsColour = Brushes.Red;
                        break;
                    case Size.S:
                        SVisible = Visibility.Visible;
                        if (availability.LowStock)
                            SColour = Brushes.Red;
                        break;
                    case Size.M:
                        MVisible = Visibility.Visible;
                        if (availability.LowStock)
                            MColour = Brushes.Red;
                        break;
                    case Size.L:
                        LVisible = Visibility.Visible;
                        if (availability.LowStock)
                            LColour = Brushes.Red;
                        break;
                    case Size.XL:
                        XlVisible = Visibility.Visible;
                        if (availability.LowStock)
                            XlColour = Brushes.Red;
                        break;
                    case Size.XXL:
                        XxlVisible = Visibility.Visible;
                        if (availability.LowStock)
                            XxlColour = Brushes.Red;
                        break;
                    case Size.LTXXS:
                        LtxxsVisible = Visibility.Visible;
                        if (availability.LowStock)
                            LtxxsColour = Brushes.Red;
                        break;
                    case Size.LTXS:
                        LtxsVisible = Visibility.Visible;
                        if (availability.LowStock)
                            LtxsColour = Brushes.Red;
                        break;
                    case Size.LTS:
                        LtsVisible = Visibility.Visible;
                        if (availability.LowStock)
                            LtsColour = Brushes.Red;
                        break;
                    case Size.LTM:
                        LtmVisible = Visibility.Visible;
                        if (availability.LowStock)
                            LtmColour = Brushes.Red;
                        break;
                    case Size.LTL:
                        LtlVisible = Visibility.Visible;
                        if (availability.LowStock)
                            LtlColour = Brushes.Red;
                        break;
                    case Size.LTXL:
                        LtxlVisible = Visibility.Visible;
                        if (availability.LowStock)
                            LtxlColour = Brushes.Red;
                        break;
                    case Size.OSFA:
                        OsfaVisible = Visibility.Visible;
                        if (availability.LowStock)
                            OsfaColour = Brushes.Red;
                        break;
                    case Size.OSFM:
                        OsfmVisible = Visibility.Visible;
                        if (availability.LowStock)
                            OsfmColour = Brushes.Red;
                        break;
                    case Size.SoldOut:
	                    break;
                    default:
	                    throw new ArgumentOutOfRangeException();
	            }
	        }
	    }

        public int Compare(ItemViewModel x, ItemViewModel y) => MyCompare(x,y);

		public int CompareTo(object obj) => MyCompare(this, (ItemViewModel)obj);

		private static int MyCompare(ItemViewModel x, ItemViewModel y)
		{
			if (Equals(x.ItemColour, Brushes.Red) && Equals(y.ItemColour, Brushes.Black))
				return -1;
			if (Equals(x.ItemColour, Brushes.Black) && Equals(y.ItemColour, Brushes.Red))
				return 1;
			if ((Equals(x.ItemColour, Brushes.Black) && Equals(y.ItemColour, Brushes.Black)))
				return string.Compare(x.Name, y.Name, StringComparison.Ordinal);
			if ((Equals(x.ItemColour, Brushes.Red) && Equals(y.ItemColour, Brushes.Red)))
				return string.Compare(x.Name, y.Name, StringComparison.Ordinal);
            return string.Compare(x.Name, y.Name, StringComparison.Ordinal);
        }

	    public event PropertyChangedEventHandler PropertyChanged;

	    [NotifyPropertyChangedInvocator]
	    protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
	    {
	        PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
	    }
	}
}
